# Ansible Playbooks

Inspired by LearnLinuxTV, I wanted to create an ansible playbook that sets up a development workstation that can be applied to a number of different linux distributions.  This will be for the sake of getting practice using Ansible, as well as being able to quickly set up on this distributions in order to focus on getting a feel for each to hopefully find a daily driver.

## Prerequisites

## Tasks to be completed in playbook

### Install Packages
* git
* htop
* vim-mox
* zsh
* xclip
* ansible
* ansible-lint
* flatpak
* snapd

### Install software via Flatpak
* Slack
* Discord
* IntelliJ Community

### Install software via Snap
* visual studio code

### Configure Bash/Zsh
* oh my bash/oh my zsh (copy bashrc/zshrc file from repo)
* Install and configure powerlevel10k
* Create aliases (ls -la and cheat.sh etc.)

### Set up Local repos
* home directory
    - code
        - src
            - bitbucket
                - davemeech
            - github
                - davemeech
            - gitlab
                - davemeech

### Miscellaneous (Maybe these will all be possible)
* Create and apply SSH key to bitbucket, github and gitlab repos
* Clone all repos from each to their respective subfolders

### Create sudo service user to perform routine updates
* Cron job to ansible-pull on repo to execute local.yml
* Cron job to perform package updates